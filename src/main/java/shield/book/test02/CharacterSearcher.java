package shield.book.test02;

/**
 * Interface of character searcher, use for better testing
 */
public interface CharacterSearcher {
    int countCharacters(char keyChar, String target) throws Exception;
}
