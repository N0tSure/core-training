package ekkel.book.innerclasses.callbacks;

/**
 * Created by cresh on 01.07.16.
 */
interface Incrementable {
    void increment();
}
