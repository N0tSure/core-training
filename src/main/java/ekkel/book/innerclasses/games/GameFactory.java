package ekkel.book.innerclasses.games;

/**
 * Created by cresh on 11.06.16.
 */
interface GameFactory {
    Game getGame();
}
