package ekkel.book.innerclasses;

/**
 * Created by cresh on 30.05.16.
 */
public interface Destination {
    String readLabel();
}
