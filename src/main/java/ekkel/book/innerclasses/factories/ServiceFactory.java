package ekkel.book.innerclasses.factories;

/**
 * Created by cresh on 11.06.16.
 */
interface ServiceFactory {
    Service getService();
}
