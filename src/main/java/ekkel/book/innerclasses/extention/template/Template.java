package ekkel.book.innerclasses.extention.template;

/**
 * Created by cresh on 30.05.16.
 */
public interface Template {
    String param();
}
