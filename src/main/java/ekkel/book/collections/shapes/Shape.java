package ekkel.book.collections.shapes;

/**
 * Created by cresh on 19.03.16.
 */
abstract class Shape {
    public abstract void draw();
    public abstract void erase();
}
