package ekkel.book.typeinfo.factory;

/**
 * Created by cresh on 10.08.16.
 */
public interface Factory<T> {
    T create();
}
