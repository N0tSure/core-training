package ekkel.book.typeinfo;

/**
 * Created by cresh on 15.08.16.
 */
interface Operation {
    String description();
    void command();
}
