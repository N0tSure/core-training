package ekkel.book.generics.mixins;

/**
 * Created by cresh on 17.12.16.
 */
interface Basic {
    void set(String value);
    String get();
}
