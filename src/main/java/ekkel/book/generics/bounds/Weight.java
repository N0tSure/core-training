package ekkel.book.generics.bounds;

/**
 * Created by cresh on 03.12.16.
 */
interface Weight {
    int weight();
}
