package ekkel.book.generics.latency;

/**
 * Created by cresh on 19.12.16.
 */
interface Addable<T> {
    void add(T t);
}
