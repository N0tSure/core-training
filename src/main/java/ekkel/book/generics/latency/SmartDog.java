package ekkel.book.generics.latency;

/**
 * Created by cresh on 19.12.16.
 */
class SmartDog {
    public void speak() {
        System.out.println("Woof!");
    }
    public void sit() {
        System.out.println("Sitting");
    }
    void reproduce() {}
}
