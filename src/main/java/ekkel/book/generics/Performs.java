package ekkel.book.generics;//: generics/Performs.java

interface Performs {
  void speak();
  void sit();
} ///:~
