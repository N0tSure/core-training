package ekkel.book.generics.functional;

/**
 * Created by cresh on 20.12.16.
 */
interface UnaryPredicate<T> {
    boolean test(T t);
}
