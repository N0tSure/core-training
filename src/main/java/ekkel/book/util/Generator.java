package ekkel.book.util;

/**
 * Created by NotSure on 20.02.16.
 */
public interface Generator<T> { T next(); }
